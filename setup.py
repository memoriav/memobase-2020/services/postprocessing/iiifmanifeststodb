from setuptools import setup

setup(
    name='iiifmanifeststodb_app',
    packages=['iiifmanifeststodb_app'],
    include_package_data=True
)
