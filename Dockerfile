FROM python:3.8

WORKDIR /iiifmanifeststodb_app
ADD iiifmanifeststodb_app /iiifmanifeststodb_app/
ADD requirements.txt /iiifmanifeststodb_app/
WORKDIR /iiifmanifeststodb_app/kubectl
WORKDIR /iiifmanifeststodb_app
RUN pip install -r requirements.txt
WORKDIR /
ADD setup.py /
RUN pip install -e .
ENTRYPOINT ["python"]
CMD ["/iiifmanifeststodb_app/main.py"]
